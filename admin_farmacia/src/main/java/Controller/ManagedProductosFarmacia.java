package Controller;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;

@Named(value = "managedProductosFarmacia")
@SessionScoped
public class ManagedProductosFarmacia implements Serializable {

    // aunque no se ocupen todos los metodos, favor de completar
    @EJB //Aqui va el Facade Local

    // si se agregan listas crear un getter con su respectiva asignacion de valores
    @PostConstruct
    private void init() { //colocar solo las inicializacion de entidades, no poner listas

    }

    public void crear() {
        try {

        } catch (Exception e) {
            System.out.println("Error al crear en: " + this.getClass().getName());
            System.out.println("Error en: " + e);
        }
    }

    public void editar() {
        try {

        } catch (Exception e) {
            System.out.println("Error al editar en: " + this.getClass().getName());
            System.out.println("Error en: " + e);
        }
    }

    public void eliminar() {
        try {

        } catch (Exception e) {
            System.out.println("Error al eliminar en: " + this.getClass().getName());
            System.out.println("Error en: " + e);
        }
    }

    public void consultarId() {
        try {

        } catch (Exception e) {
            System.out.println("Error al consultar ID en: " + this.getClass().getName());
            System.out.println("Error en: " + e);
        }
    }

    public void limpiar() {
        try {

        } catch (Exception e) {
            System.out.println("Error al limpiar en: " + this.getClass().getName());
            System.out.println("Error en: " + e);
        }
    }
}
