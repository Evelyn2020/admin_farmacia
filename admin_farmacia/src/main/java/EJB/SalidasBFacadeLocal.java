/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.SalidasB;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesar.murciausam
 */
@Local
public interface SalidasBFacadeLocal {

    void create(SalidasB salidasB);

    void edit(SalidasB salidasB);

    void remove(SalidasB salidasB);

    SalidasB find(Object id);

    List<SalidasB> findAll();

    List<SalidasB> findRange(int[] range);

    int count();
    
}
