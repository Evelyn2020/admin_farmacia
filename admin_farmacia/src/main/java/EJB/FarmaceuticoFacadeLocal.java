/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.Farmaceutico;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author evelyn.andradeusam
 */
@Local
public interface FarmaceuticoFacadeLocal {

    void create(Farmaceutico farmaceutico);

    void edit(Farmaceutico farmaceutico);

    void remove(Farmaceutico farmaceutico);

    Farmaceutico find(Object id);

    List<Farmaceutico> findAll();

    List<Farmaceutico> findRange(int[] range);

    int count();
    
}
