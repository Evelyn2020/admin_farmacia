/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.InventarioFarmacia;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesar.murciausam
 */
@Local
public interface InventarioFarmaciaFacadeLocal {

    void create(InventarioFarmacia inventarioFarmacia);

    void edit(InventarioFarmacia inventarioFarmacia);

    void remove(InventarioFarmacia inventarioFarmacia);

    InventarioFarmacia find(Object id);

    List<InventarioFarmacia> findAll();

    List<InventarioFarmacia> findRange(int[] range);

    int count();
    
}
