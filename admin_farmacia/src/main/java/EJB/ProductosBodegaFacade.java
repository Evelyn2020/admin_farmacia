/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.ProductosBodega;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author cesar.murciausam
 */
@Stateless
public class ProductosBodegaFacade extends AbstractFacade<ProductosBodega> implements ProductosBodegaFacadeLocal {

    @PersistenceContext(unitName = "farmac_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductosBodegaFacade() {
        super(ProductosBodega.class);
    }
    
}
