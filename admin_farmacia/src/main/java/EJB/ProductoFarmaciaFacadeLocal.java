/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.ProductoFarmacia;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesar.murciausam
 */
@Local
public interface ProductoFarmaciaFacadeLocal {

    void create(ProductoFarmacia productoFarmacia);

    void edit(ProductoFarmacia productoFarmacia);

    void remove(ProductoFarmacia productoFarmacia);

    ProductoFarmacia find(Object id);

    List<ProductoFarmacia> findAll();

    List<ProductoFarmacia> findRange(int[] range);

    int count();
    
}
