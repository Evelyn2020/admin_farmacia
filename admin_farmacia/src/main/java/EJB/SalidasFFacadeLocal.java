/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.SalidasF;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesar.murciausam
 */
@Local
public interface SalidasFFacadeLocal {

    void create(SalidasF salidasF);

    void edit(SalidasF salidasF);

    void remove(SalidasF salidasF);

    SalidasF find(Object id);

    List<SalidasF> findAll();

    List<SalidasF> findRange(int[] range);

    int count();
    
}
