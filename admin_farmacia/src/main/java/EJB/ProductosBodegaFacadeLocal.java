/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.ProductosBodega;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesar.murciausam
 */
@Local
public interface ProductosBodegaFacadeLocal {

    void create(ProductosBodega productosBodega);

    void edit(ProductosBodega productosBodega);

    void remove(ProductosBodega productosBodega);

    ProductosBodega find(Object id);

    List<ProductosBodega> findAll();

    List<ProductosBodega> findRange(int[] range);

    int count();
    
}
