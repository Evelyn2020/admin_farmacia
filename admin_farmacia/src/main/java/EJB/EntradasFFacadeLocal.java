/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.EntradasF;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesar.murciausam
 */
@Local
public interface EntradasFFacadeLocal {

    void create(EntradasF entradasF);

    void edit(EntradasF entradasF);

    void remove(EntradasF entradasF);

    EntradasF find(Object id);

    List<EntradasF> findAll();

    List<EntradasF> findRange(int[] range);

    int count();
    
}
