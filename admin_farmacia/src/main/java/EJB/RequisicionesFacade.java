/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.Requisiciones;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author cesar.murciausam
 */
@Stateless
public class RequisicionesFacade extends AbstractFacade<Requisiciones> implements RequisicionesFacadeLocal {

    @PersistenceContext(unitName = "farmac_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RequisicionesFacade() {
        super(Requisiciones.class);
    }
    
}
