/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.InventarioBodega;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesar.murciausam
 */
@Local
public interface InventarioBodegaFacadeLocal {

    void create(InventarioBodega inventarioBodega);

    void edit(InventarioBodega inventarioBodega);

    void remove(InventarioBodega inventarioBodega);

    InventarioBodega find(Object id);

    List<InventarioBodega> findAll();

    List<InventarioBodega> findRange(int[] range);

    int count();
    
}
