/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.Requisiciones;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author evelyn.andradeusam
 */
@Local
public interface RequisicionesFacadeLocal {

    void create(Requisiciones requisiciones);

    void edit(Requisiciones requisiciones);

    void remove(Requisiciones requisiciones);

    Requisiciones find(Object id);

    List<Requisiciones> findAll();

    List<Requisiciones> findRange(int[] range);

    int count();
    
}
