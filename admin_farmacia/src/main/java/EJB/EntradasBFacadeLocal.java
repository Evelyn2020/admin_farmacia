/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.EntradasB;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesar.murciausam
 */
@Local
public interface EntradasBFacadeLocal {

    void create(EntradasB entradasB);

    void edit(EntradasB entradasB);

    void remove(EntradasB entradasB);

    EntradasB find(Object id);

    List<EntradasB> findAll();

    List<EntradasB> findRange(int[] range);

    int count();
    
}
