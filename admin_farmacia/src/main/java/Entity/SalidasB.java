package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author silvia.estupinianusa
 */
@Entity
@Table(name = "salidas_b")
public class SalidasB implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_registro;

    @JoinColumn(name = "id_requisicion", referencedColumnName = "id_requisicion")
    @ManyToOne
    private Requisiciones requisiciones;

    @Column(name = "fecha_lote")
    @Temporal(TemporalType.DATE)
    private Date fecha_lote;

    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fecha_caducidad;

    public SalidasB() {
    }

    public int getId_registro() {
        return id_registro;
    }

    public void setId_registro(int id_registro) {
        this.id_registro = id_registro;
    }

    public Requisiciones getRequisiciones() {
        return requisiciones;
    }

    public void setRequisiciones(Requisiciones requisiciones) {
        this.requisiciones = requisiciones;
    }

    public Date getFecha_lote() {
        return fecha_lote;
    }

    public void setFecha_lote(Date fecha_lote) {
        this.fecha_lote = fecha_lote;
    }

    public Date getFecha_caducidad() {
        return fecha_caducidad;
    }

    public void setFecha_caducidad(Date fecha_caducidad) {
        this.fecha_caducidad = fecha_caducidad;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id_registro;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SalidasB other = (SalidasB) obj;
        if (this.id_registro != other.id_registro) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Salidas_b{" + "id_registro=" + id_registro + '}';
    }

}
