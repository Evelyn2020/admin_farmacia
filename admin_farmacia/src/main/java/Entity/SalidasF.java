/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author evelyn.andradeusam
 */

@Entity
@Table(name = "salidas_f")
public class SalidasF implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_registro;
    
    @Column(name = "fecha_lote")
    @Temporal(TemporalType.DATE)
    private Date fecha_lote;
    
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fecha_vencimiento;
    
    @JoinColumn(name = "id_venta", referencedColumnName = "id_venta")
    @ManyToOne
    private Ventas ventas;

    public SalidasF() {
    }

    public int getId_registro() {
        return id_registro;
    }

    public void setId_registro(int id_registro) {
        this.id_registro = id_registro;
    }

    public Date getFecha_lote() {
        return fecha_lote;
    }

    public void setFecha_lote(Date fecha_lote) {
        this.fecha_lote = fecha_lote;
    }

    public Date getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(Date fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public Ventas getVentas() {
        return ventas;
    }

    public void setVentas(Ventas ventas) {
        this.ventas = ventas;
    }

  

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.id_registro;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SalidasF other = (SalidasF) obj;
        if (this.id_registro != other.id_registro) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Salidas_f{" + "id_registro=" + id_registro + '}';
    }
    
    
    
}
