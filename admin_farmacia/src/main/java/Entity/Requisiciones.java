
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author silvia.estupinianusa
 */
@Entity
@Table(name="requisiciones")
public class Requisiciones implements Serializable{
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_requisicion;
    
    @Column(name="descripcion")
    private String descripcion; 
    
    @JoinColumn(name="id_farmaceutico", referencedColumnName = "id_farmaceutico")
    @ManyToOne
    private Farmaceutico Farmaceutico;  
    
    @Column(name="fecha_requisicion")
    @Temporal(TemporalType.DATE)
    private Date fecha_requisicion;       

    public Requisiciones() {
    }

    public int getId_requisicion() {
        return id_requisicion;
    }

    public void setId_requisicion(int id_requisicion) {
        this.id_requisicion = id_requisicion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Farmaceutico getFarmaceutico() {
        return Farmaceutico;
    }

    public void setFarmaceutico(Farmaceutico Farmaceutico) {
        this.Farmaceutico = Farmaceutico;
    }

    public Date getFecha_requisicion() {
        return fecha_requisicion;
    }

    public void setFecha_requisicion(Date fecha_requisicion) {
        this.fecha_requisicion = fecha_requisicion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id_requisicion;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Requisiciones other = (Requisiciones) obj;
        if (this.id_requisicion != other.id_requisicion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Requisiciones{" + "id_requisicion=" + id_requisicion + '}';
    }

    
}
