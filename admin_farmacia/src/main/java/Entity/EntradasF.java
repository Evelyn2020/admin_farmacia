/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author evelyn.andradeusam
 */
@Entity
@Table(name = "entradas_f")
public class EntradasF implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_registro;

    @Column(name = "fecha_lote")
    @Temporal(TemporalType.DATE)
    private Date fecha_lote;

    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fecha_vencimiento;

    @ManyToOne
    @JoinColumn(name = "id_requisicion", referencedColumnName = "id_requisicion")
    private Requisiciones requisiones;

    public EntradasF() {
    }

    public int getId_registro() {
        return id_registro;
    }

    public void setId_registro(int id_registro) {
        this.id_registro = id_registro;
    }

    public Date getFecha_lote() {
        return fecha_lote;
    }

    public void setFecha_lote(Date fecha_lote) {
        this.fecha_lote = fecha_lote;
    }

    public Date getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(Date fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public Requisiciones getRequisiones() {
        return requisiones;
    }

    public void setRequisiones(Requisiciones requisiones) {
        this.requisiones = requisiones;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.id_registro;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntradasF other = (EntradasF) obj;
        if (this.id_registro != other.id_registro) {
            return false;
        }
        return true;
    }

}
