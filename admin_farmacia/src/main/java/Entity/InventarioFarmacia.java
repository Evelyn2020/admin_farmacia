/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author evelyn.andradeusam
 */
@Entity
@Table(name = "inventario_farmacia")
public class InventarioFarmacia implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int Id_registro;
    
    @Column(name = "entrada")
    private int entrada;
    
    @Column(name = "salida")
    private int salida;
    
    @Column(name = "cantidad")
    private int cantidad;
    
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date fecha_registro;
    
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne
    private ProductoFarmacia producto_farmacia;

    public InventarioFarmacia() {
    }

    public int getId_registro() {
        return Id_registro;
    }

    public void setId_registro(int Id_registro) {
        this.Id_registro = Id_registro;
    }

    public int getEntrada() {
        return entrada;
    }

    public void setEntrada(int entrada) {
        this.entrada = entrada;
    }

    public int getSalida() {
        return salida;
    }

    public void setSalida(int salida) {
        this.salida = salida;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public ProductoFarmacia getProducto_farmacia() {
        return producto_farmacia;
    }

    public void setProducto_farmacia(ProductoFarmacia producto_farmacia) {
        this.producto_farmacia = producto_farmacia;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.Id_registro;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InventarioFarmacia other = (InventarioFarmacia) obj;
        if (this.Id_registro != other.Id_registro) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Inventario_farmacia{" + "Id_registro=" + Id_registro + '}';
    }
    
    
    
    
}
