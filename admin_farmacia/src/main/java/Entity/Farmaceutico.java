/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author evelyn.andradeusam
 */

@Entity
@Table(name = "farmaceutico")
public class Farmaceutico implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_farmaceutico;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "apellido")
    private String apellido;
    
    @Column(name = "email")
    private String email;
    
     @Column(name = "dui")
     private String DUI;

    public Farmaceutico() {
    }

    public int getId_farmaceutico() {
        return id_farmaceutico;
    }

    public void setId_farmaceutico(int id_farmaceutico) {
        this.id_farmaceutico = id_farmaceutico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDUI() {
        return DUI;
    }

    public void setDUI(String DUI) {
        this.DUI = DUI;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id_farmaceutico;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Farmaceutico other = (Farmaceutico) obj;
        if (this.id_farmaceutico != other.id_farmaceutico) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Farmaceutico{" + "id_farmaceutico=" + id_farmaceutico + '}';
    }
     
     
    
}
